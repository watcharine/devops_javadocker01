pipeline {

    parameters { 
        choice( name: 'DEPLOY_ENV', choices: ['DEV','SIT'], description: 'Deploy to environment?')
    }
    
    agent any

    tools { 
        maven 'maven363' 
        jdk 'jdk14' 
    }

        /*dockerfile {
            filename 'Dockerfile'
            dir 'docker-environment'
            args '-v $PWD:/var/java-project \
                -v /var/lib/jenkins/tools/hudson.plugins.sonar.SonarRunnerInstallation:/var/lib/jenkins/tools/hudson.plugins.sonar.SonarRunnerInstallation \
                -v /var/run/docker.sock:/var/run/docker.sock'
        }*/
    
    
    stages {

        stage('Initialize') {
            steps {
                script {
                    def props = readProperties file: '.env'
                    //props.each{ k,v -> println "$k = $v" }
                    props.each{ k,v -> env."${k}" = "${v}" }
					MESSAGE_TEMPLATE = "$JOB_NAME Build #$BUILD_ID " + env['MESSAGE_TEMPLATE'] + "\r\n ${BUILD_URL}input"
					DEV_HOST = "${DEV_HOST}" + "." + "${DEV_ZONE}"
					SIT_HOST = "${SIT_HOST}" + "." + "${SIT_ZONE}"
					
                    IMAGE_VERSION = "${PROJECT_VERSION}"
                    
                    echo "${IMAGE_VERSION}"
                    
                    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'jfrog-credentials', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                        sh "jfrog rt config --url=${JFROG_ARTIFACTORY_URL} --user=$USERNAME --password=$PASSWORD"
                    }
                    
                    /*dir("./docker-environment"){
                        def settings = readFile('settings.xml')
                        withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: JFROG_CREDENTIALS, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                            settings = settings.replace("{JFrogUsername}", "$USERNAME")
                            settings = settings.replace("{JFrogPassword}", "$PASSWORD")
                            settings = settings.replace("{JFrogVirtualURL}", "${JFROG_ARTIFACTORY_URL}/${JFROG_VIRTUAL_REPO}")
                        }
                        writeFile file:"settings.xml", text:settings
                        sh 'cat settings.xml'
                        sh 'mv settings.xml /usr/share/maven/conf/'
                    }*/
                    
                    // Child Branch Decision
                    env.BRANCH_ENV = 1 // Default Environment : DEV
                    env.DO_SIT = false
                    
                    // Developer branch
                    if(params.DEPLOY_ENV == "DEV"){ 
                        env.BRANCH_ENV = 1 // DEV 
                    }
                    else if(params.DEPLOY_ENV == "SIT"){ 
                        env.BRANCH_ENV = 2 // DEV SIT 
                        env.DO_SIT = true
                    }
                }
            }
        }
        
        /*stage('Test') {
            steps {
                sh "go test ./service -v -coverprofile=coverage.out"
                sh "ls -lt"
            }
        }*/

        stage('Maven Test') {
             steps {
                 sh 'mvn test'
             }
             /*post {
                 always {
                     junit 'target/test-classes/*.xml'
                 }
             }*/
         }

        stage('Maven Deploy') {
            steps {
                script{
                    branchName = env.GIT_BRANCH.replace("origin/","") 
                    echo "${branchName}"
                    def pom = readMavenPom file: 'pom.xml'
                    ORIGINAL_ARTIFACT_ID = pom.artifactId
                    ORIGINAL_ARTIFACT_VERSION = pom.version
                    pom.version = pom.version.toString() + "-$BUILD_ID"
                    // pom.distributionManagement.repository.url = '' // clear default value
                    // pom.distributionManagement.repository.url = pom.distributionManagement.repository.url.toString + "${JFROG_ARTIFACTORY_URL}/${JFROG_LOCAL_REPO}"
                    // ****** Change project name when branch not master ******
                    if (env.GIT_BRANCH != "${BRANCH_MASTER_NAME}") {
                        pom.artifactId = pom.artifactId.toString() + "-${branchName}"
                    }
                    
                    writeMavenPom model: pom
                    sh 'cat pom.xml'   
		    //sh 'pwd'		 
                        // sh "mvn versions:set -DnewVersion=${version}-$BUILD_ID -DnewArtifactId=${artifactId}-${branchName}" //
                    sh "mvn install:install-file -Dfile=./lib/ScJDBC-3.1.7.jar -DgroupId=sc.jdbc -DartifactId=scjdbc -Dversion=3.1.7.0 -Dpackaging=jar"    
                    //sh "cp ./src/main/java/com/marcotechnology/springbootdemojar/SpringbootdemojarApplication.java ./target/classes" 
                    //sh "cp ./src/main/java/com/marcotechnology/springbootdemojar/SpringbootdemojarController.java ./target/classes"
                    sh "mvn deploy"
                }
            }
        }

        stage('Build') {
            steps {
                sh "docker build -t java-spring-boot"
                sh "ls -lt"
            }
        }
        
        stage('Push to Artifact') {
            steps {
                sh "mkdir -p build \
                && mv ${PROJECT_NAME} ./build/ "
                
                dir("./build")  {
                    sh "zip ${PROJECT_NAME}_v${PROJECT_VERSION}.zip ${PROJECT_NAME}"
                    sh "ls -lt"
                    sh "jfrog rt u ${PROJECT_NAME}_v${PROJECT_VERSION}.zip ${JFROG_LOCAL_REPO}/${PROJECT_NAME}_v${PROJECT_VERSION}.zip"
                }
            }
        }
        
        stage('Create Docker Image') {
            steps {
                sh "rm -rf dockerfile-workspace"
                sh "mkdir -p dockerfile-workspace"
                sh "cp main.go Dockerfile ./dockerfile-workspace"
                dir("./dockerfile-workspace") {
                    sh "jfrog rt dl ${JFROG_LOCAL_REPO}/${PROJECT_NAME}_v${PROJECT_VERSION}.zip"
                    sh "unzip ${PROJECT_NAME}_v${PROJECT_VERSION}.zip"
                    sh "rm -rf ${PROJECT_NAME}_v${PROJECT_VERSION}.zip"
                    sh "ls -laish"                 
                    script{
                        docker.withRegistry(DOCKER_REGISTRY_PROTOCAL + DOCKER_REGISTRY_URL, DOCKER_REGISTRY_CREDENTIALS) {
                            docker.build("${DOCKER_REGISTRY_BASE}/${PROJECT_NAME}:v${PROJECT_VERSION}").push()
                        }      
                        sh "docker rmi ${DOCKER_REGISTRY_BASE}/${PROJECT_NAME}:v${PROJECT_VERSION}"
                        sh "docker rmi ${DOCKER_REGISTRY_URL}/${DOCKER_REGISTRY_BASE}/${PROJECT_NAME}:v${PROJECT_VERSION}"              
                    }
                }
            }
        }
        
        stage('Deploy Environment (DEV)') {
            steps {
                sh "docker rm -f ${PROJECT_NAME} || true"
                sh "docker rmi ${PROJECT_NAME}:latest || true"
                sh "docker pull ${DOCKER_REGISTRY_URL}/${DOCKER_REGISTRY_BASE}/${PROJECT_NAME}:v${PROJECT_VERSION}"
                sh "docker image tag ${DOCKER_REGISTRY_URL}/${DOCKER_REGISTRY_BASE}/${PROJECT_NAME}:v${PROJECT_VERSION} ${PROJECT_NAME}:latest"
                sh "docker rmi ${DOCKER_REGISTRY_URL}/${DOCKER_REGISTRY_BASE}/${PROJECT_NAME}:v${PROJECT_VERSION}"
                sh "docker run --detach --publish ${DEV_PORT}:18080 --name ${PROJECT_NAME} ${PROJECT_NAME}:latest"
                /*script{
                    docker.withRegistry(DOCKER_REGISTRY_PROTOCAL + DOCKER_REGISTRY_URL, DOCKER_REGISTRY_CREDENTIALS) {
                        docker.image("${DOCKER_REGISTRY_BASE}/${PROJECT_NAME}:v${PROJECT_VERSION}").withRun("-d -p 18002:18080 --name ${PROJECT_NAME}")
                    }
                }*/
            }
        }
        
        stage('Robot Test (DEV)') {
            steps {
                script{
                    // Master
                    initialRobotFile("${ROBOT_FILE}","${DEV_HOST}","${DEV_PORT}")
					robotTest("${ROBOT_FILE}",'dev')
                }
            }
        }
        
        stage('JMeter Test (DEV)') {
            steps {
                script{
                    // Master
				 	initialJMeterFile("${JMETER_DEV_FILE}","${DEV_HOST}","${DEV_PORT}")
					jmeterTest("${JMETER_DEV_FILE}",'dev')
                }
            }
        }
        
        stage('Code Analysis') {
            steps{
                script{
                    def scannerHome = tool 'Sonar Scan';
                    withSonarQubeEnv('SonarQube') {
                        echo "Analysis with SonarQube"
                        sh "${scannerHome}/bin/sonar-scanner"
                    }
                }
            }                            
        }
        
        stage("Quality Gate") {
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    // Parameter indicates whether to set pipeline to UNSTABLE if Quality Gate fails
                    // true = set pipeline to UNSTABLE, false = don't
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        
        stage('Deploy Environment (SIT)') {
            when {  expression { return env.DO_SIT.toBoolean() == true; } }
            steps {
                initialDeploymentFile("${SIT_DEPLOY_FILE}")
                dir("./deployment"){
                    sh "scp ./deployment-sit.sh tony@${SIT_HOST}:/home/tony/"
                    sh "ssh tony@${SIT_HOST} sh ${SIT_DEPLOY_FILE}"
                }
                //sh "ssh tony@${SIT_HOST} sudo docker rm -f ${PROJECT_NAME} || true"
                //sh "ssh tony@${SIT_HOST} sudo docker rmi ${PROJECT_NAME}:latest || true"
                //sh "ssh tony@${SIT_HOST} sudo docker pull ${DOCKER_REGISTRY_URL}/${DOCKER_REGISTRY_BASE}/${PROJECT_NAME}:v${PROJECT_VERSION}"
                //sh "ssh tony@${SIT_HOST} sudo docker image tag ${DOCKER_REGISTRY_URL}/${DOCKER_REGISTRY_BASE}/${PROJECT_NAME}:v${PROJECT_VERSION} ${PROJECT_NAME}:latest"
                //sh "ssh tony@${SIT_HOST} sudo docker rmi ${DOCKER_REGISTRY_URL}/${DOCKER_REGISTRY_BASE}/${PROJECT_NAME}:v${PROJECT_VERSION}"
                //sh "ssh tony@${SIT_HOST} sudo docker run --detach --publish ${SIT_PORT}:18080 --name ${PROJECT_NAME} ${PROJECT_NAME}:latest"
                /*script{
                    docker.withRegistry(DOCKER_REGISTRY_PROTOCAL + DOCKER_REGISTRY_URL, DOCKER_REGISTRY_CREDENTIALS) {
                        docker.image("${DOCKER_REGISTRY_BASE}/${PROJECT_NAME}:v${PROJECT_VERSION}").withRun("-d -p 18002:18080 --name ${PROJECT_NAME}")
                    }
                }*/
            }
        }
        
        stage('Robot Test (SIT)') {
            when {  expression { return env.DO_SIT.toBoolean() == true; } }
            steps {
                script{
                    // Master
                    initialRobotFile("${ROBOT_FILE}","${SIT_HOST}","${SIT_PORT}")
					robotTest("${ROBOT_FILE}",'sit')
                }
            }
        }
        
        stage('JMeter Test (SIT)') {
            when {  expression { return env.DO_SIT.toBoolean() == true; } }
            steps {
                script{
                    // Master
				 	initialJMeterFile("${JMETER_SIT_FILE}","${SIT_HOST}","${SIT_PORT}")
					jmeterTest("${JMETER_SIT_FILE}",'sit')
                }
            }
        }
    }
    
    post { 
        always {
            script {
                dir("./robot-result"){
                    if(fileExists("/")){
                        step(
                            [
                                $class              : 'RobotPublisher',
                                outputPath          : '',
                                outputFileName      : "**/output.xml",
                                reportFileName      : '**/report.html',
                                logFileName         : '**/log.html',
                                disableArchiveOutput: false,
                                passThreshold       : 100,
                                unstableThreshold   : 95,
                                otherFiles          : "**/*.png,**/*.jpg",
                            ]
                        )
                    }
                }
                
                dir("./jmeter-result"){
                    if(fileExists("/")){
                        // Publish JMeter Report
                        perfReport "**/*.log"
                    }
                }
                
                //deleteDir()
                
            }
        }
        
        success{
            echo "Post all stage success"
        }
        
        failure{
            echo "Post all stage failed"
        }
    }
}

def initialDeploymentFile(fileName){
	script{
		dir("./deployment"){
            def deployment = readFile("${fileName}")
            deployment = deployment.replace("{SIT_PORT}", "${SIT_PORT}")
            deployment = deployment.replace("{PROJECT_NAME}", "${PROJECT_NAME}")
            deployment = deployment.replace("{PROJECT_VERSION}", "${PROJECT_VERSION}")
            deployment = deployment.replace("{DOCKER_REGISTRY_URL}", "${DOCKER_REGISTRY_URL}")
            deployment = deployment.replace("{DOCKER_REGISTRY_BASE}", "${DOCKER_REGISTRY_BASE}")
            writeFile file:"${fileName}", text:deployment
            sh "cat ${fileName}"
		}
	}
}

def initialRobotFile(fileName,host,port){
	script{
		dir("./robot-test"){
			def robot = readFile("${fileName}")
			robot = robot.replace("{ROBOT_HOST}", "http://" + "${host}")
			robot = robot.replace("{ROBOT_PORT}", "${port}")
			writeFile file:"${fileName}", text:robot
            sh "cat ${fileName}"
		}
	}
}

def initialJMeterFile(fileName,host,port){
	script{
		dir("./jmeter-test"){
			def jmeter = readFile("${fileName}")
			jmeter = jmeter.replace("{JMETER_HOST}", "http://" + "${host}")
			jmeter = jmeter.replace("{JMETER_PORT}", "${port}")
			writeFile file:"${fileName}", text:jmeter
			sh "cat ${fileName}"
		}
	}
}

def robotTest(file,outputFolder){
    sh "mkdir -p robot-result/${outputFolder}" // Make Directory wait for result
    echo "${file}"
    echo "${outputFolder}"
    build job: "${ROBOT_PIPELINE}",  
          parameters: [ string(name: 'FROM_JOB_NAME', value: "$JOB_NAME" ), 
                        string(name: 'RUN_FILE', value: "${file}" ), 
                        string(name: 'RESULT_FOLDER', value: "${outputFolder}" )
                    ], wait: true
    dir("./robot-result/${outputFolder}"){
        sh 'ls'
    }
}

def jmeterTest(file,outputFolder){
    sh "mkdir -p jmeter-result/${outputFolder}" // Make Directory wait for result
    echo "${file}"
    echo "${outputFolder}"
    build job: "${JMETER_PIPELINE}",  
          parameters: [ string(name: 'FROM_JOB_NAME', value: "$JOB_NAME" ), 
                        string(name: 'RUN_FILE', value: "${file}" ), 
                        string(name: 'RESULT_FOLDER', value: "${outputFolder}" )
                    ], wait: true
    dir("./jmeter-result/${outputFolder}"){
        sh 'ls'
    }
}